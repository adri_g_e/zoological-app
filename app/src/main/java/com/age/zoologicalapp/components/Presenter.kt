package com.age.zoologicalapp.components

import com.age.zoologicalapp.ui.View


interface Presenter {

    fun setView(view: View)
    fun detachView()
    fun getAnimals()
    fun changeFriendships()
    fun addHourToDay()
    fun resetDay()
    fun changeLiveOneDayStatus()
    fun isLiveOneDayEnabled() : Boolean
}