package com.age.zoologicalapp.components

import com.age.zoologicalapp.data.Animal


interface Model {

    fun createAnimals() : MutableList<Animal>
}