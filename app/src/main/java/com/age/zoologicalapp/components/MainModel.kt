package com.age.zoologicalapp.components

import com.age.zoologicalapp.data.Animal
import com.age.zoologicalapp.data.Chicken
import com.age.zoologicalapp.data.Dog
import com.age.zoologicalapp.data.Parrot

// No exist REPO for get data.
class MainModel : Model {

    var animals : MutableList<Animal> = ArrayList()


    override fun createAnimals(): MutableList<Animal> {
        if (animals.isEmpty()) {
            createDogs()
            createParrots()
            createChickens()
        }
        return animals
    }


    private fun createDogs() {
        val dog1 = Dog("Killian", "Meat", "Hunting dog", "https://firebasestorage.googleapis.com/v0/b/zoologicalapp.appspot.com/o/ic_dog.png?alt=media&token=123e06d7-ad5b-4106-8e73-db728c9aaed0")
        val dog2 = Dog("Peter", "Pedigree", "Sport dog", "https://firebasestorage.googleapis.com/v0/b/zoologicalapp.appspot.com/o/ic_dog_2.png?alt=media&token=e1fe9cd7-8569-4f90-821f-7fccba32503e")
        val dog3 = Dog("Rocky", "Fresh meat", "Working dog", "https://firebasestorage.googleapis.com/v0/b/zoologicalapp.appspot.com/o/ic_dog_3.png?alt=media&token=64f47638-708f-4792-8ce8-1c29c710ac18")
        animals.add(dog1)
        animals.add(dog2)
        animals.add(dog3)
    }


    private fun createParrots() {
        val parrot1 = Parrot("Parrot one", "Grain", false, 0.25, "https://firebasestorage.googleapis.com/v0/b/zoologicalapp.appspot.com/o/ic_parrot.png?alt=media&token=25f2baaa-da86-4142-a8fb-10f06ee53956")
        val parrot2 = Parrot("Parrot two", "Corn", true, 0.5, "https://firebasestorage.googleapis.com/v0/b/zoologicalapp.appspot.com/o/ic_parrot_2.png?alt=media&token=3a1f7191-6b39-4377-b94c-633640ffa1a6")
        animals.add(parrot1)
        animals.add(parrot2)
    }


    private fun createChickens() {
        val chicken1 = Chicken("Chicken one", "Corn", 0.75, true, "https://firebasestorage.googleapis.com/v0/b/zoologicalapp.appspot.com/o/ic_chicken.png?alt=media&token=39652035-eda6-4777-8b0b-c83740489e72")
        val chicken2 = Chicken("Rocky", "Corn", 0.75, false, "https://firebasestorage.googleapis.com/v0/b/zoologicalapp.appspot.com/o/ic_chicken_2.png?alt=media&token=88f262f6-d5e5-4c89-9ff6-3b93cb0cb028")
        animals.add(chicken1)
        animals.add(chicken2)
    }
}