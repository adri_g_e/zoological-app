package com.age.zoologicalapp.components

import androidx.annotation.NonNull
import com.age.zoologicalapp.data.Animal
import com.age.zoologicalapp.ui.View


class MainPresenter constructor(val model: Model) : Presenter {

    private val HOURS_OF_DAY = 24
    private var view: View? = null
    private var animals = mutableListOf<Animal>()
    private var animalsToEstablishFriendship = mutableListOf<Animal>()
    private var hourOfday = 0
    private var isLiveOneDayEnabled = true


    override fun setView(@NonNull view: View) {
        this.view = view
    }


    override fun detachView() {
        this.view = null
    }


    override fun resetDay() {
        hourOfday = 0
        notifyHourChangedToView()
    }


    override fun addHourToDay() {
        sumHourToDay()
        if (isNewDay()) {
            resetDay()
            updateToNewDay()
        }
    }


    private fun sumHourToDay() {
        hourOfday += 1
        notifyHourChangedToView()
    }


    private fun isNewDay(): Boolean {
        return hourOfday == HOURS_OF_DAY
    }


    private fun updateToNewDay() {
        changeFriendships()
    }


    override fun changeFriendships() {
        breakFriendships()
        createFriendships()
        notifyDayChangedToView()
    }


    fun breakFriendships() {
        animals.forEach { animal ->
            if (animalHasFriends(animal)) {
                val animalIndexToRemove = animal.friendships.indices.random()
                val randomAnimal = animal.friendships[animalIndexToRemove]
                breakFriendship(animal, randomAnimal)
            }
        }
    }


    private fun animalHasFriends(animal: Animal): Boolean {
        return animal.friendships.isNotEmpty()
    }


    private fun breakFriendship(animal1: Animal, animal2: Animal) {
        animal1.removeFriendship(animal2)
        animal2.removeFriendship(animal1)
    }


    private fun createFriendships() {
        resetAnimalsPendingToFriendships()
        animals.forEach { animal ->
            if (canStablishFriendship()) {
                val randomAnimal = getRandomAnimal(animal)
                establishFriendship(animal, randomAnimal)
                val animalsWithFriendship: Array<Animal> = arrayOf(animal, randomAnimal)
                removeAnimalsFromPendingToFriendList(animalsWithFriendship)
            }
        }
    }


    private fun canStablishFriendship(): Boolean {
        return animalsToEstablishFriendship.size > 1
    }


    private fun getRandomAnimal(excludeAnimal: Animal) : Animal {
        var animalSelected: Animal? = null
        while (animalSelected == null) {
            val animalIndex = animalsToEstablishFriendship.indices.random()
            val animal = animalsToEstablishFriendship[animalIndex]
            if (animal != excludeAnimal) {
                animalSelected = animal
            }
        }
        return animalSelected
    }


    private fun establishFriendship(animal1: Animal, animal2: Animal) {
        animal1.addFriendship(animal2)
        animal2.addFriendship(animal1)
    }


    private fun removeAnimalsFromPendingToFriendList(animalsToRemove: Array<Animal>) {
        animalsToRemove.forEach { animal ->
            animalsToEstablishFriendship.remove(animal)
        }
    }


    private fun notifyHourChangedToView() {
        if (view != null) {
            if (hourOfday < 13) {
                val hourOfDayWithMeridium = "$hourOfday AM"
                view!!.onHourOfDayUpdated(hourOfDayWithMeridium)
            } else {
                val hourOfDayWithMeridium = "$hourOfday PM"
                view!!.onHourOfDayUpdated(hourOfDayWithMeridium)
            }
        }
    }


    override fun getAnimals() {
        this.animals = model.createAnimals()
        notifyAnimalsCreatedToView()
    }


    private fun notifyAnimalsCreatedToView() {
        if (view != null)
            view!!.onAnimalsCreated(animals)
    }


    private fun resetAnimalsPendingToFriendships() {
        animalsToEstablishFriendship.clear()
        animalsToEstablishFriendship.addAll(animals)
    }


    private fun notifyDayChangedToView() {
        if (view != null)
            view!!.onDayChanged(animals)
    }


    override fun changeLiveOneDayStatus() {
        isLiveOneDayEnabled = !isLiveOneDayEnabled
        notifyLiveOneDayStatusToView()
    }


    private fun notifyLiveOneDayStatusToView() {
        if (view != null) {
            if (isLiveOneDayEnabled) {
                resetDay()
                view!!.enableLiveOneDay()
            } else {
                view!!.disableLiveOneDay()
            }
        }
    }


    override fun isLiveOneDayEnabled(): Boolean {
        return isLiveOneDayEnabled
    }

}