package com.age.zoologicalapp.ui

import android.os.Bundle
import android.os.SystemClock
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.age.zoologicalapp.R
import com.age.zoologicalapp.components.Presenter
import com.age.zoologicalapp.data.Animal
import com.age.zoologicalapp.injection.DaggerMainComponent
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject
import com.google.android.material.snackbar.Snackbar
import android.view.Menu
import android.view.MenuItem



class MainActivity : AppCompatActivity(), View {

    private val GRID_COLUMNS = 2

    @Inject lateinit var presenter: Presenter
    private lateinit var adapter: AnimalsAdapter
    private lateinit var gridLayoutManager: GridLayoutManager
    private lateinit var gridItemDecoration: GridItemDecoration
    private var menuItemLOD: MenuItem? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initialize()
    }


    private fun initialize() {
        injectDependencies()
        initializeToolbar()
        initializeAdapter()
        initializeViewManager()
        initializeGridItemDecoration()
        initializeChronometer()
        configureRecyclerView()
    }


    private fun injectDependencies() {
        DaggerMainComponent.create().inject(this)
    }


    private fun initializeToolbar() {
        setSupportActionBar(toolbar)
    }


    private fun initializeAdapter() {
        adapter = AnimalsAdapter()
    }


    private fun initializeViewManager() {
        gridLayoutManager = GridLayoutManager(this, GRID_COLUMNS)
        gridLayoutManager.orientation = RecyclerView.VERTICAL
    }


    private fun initializeGridItemDecoration() {
        val largePadding = resources.getDimensionPixelSize(R.dimen.grid_large_spacing)
        val smallPadding = resources.getDimensionPixelSize(R.dimen.grid_small_padding)
        gridItemDecoration = GridItemDecoration(largePadding, smallPadding)
    }


    private fun initializeChronometer() {
        chronometer.base = SystemClock.elapsedRealtime()
        chronometer.setOnChronometerTickListener {
            presenter.addHourToDay()
        }
    }


    private fun configureRecyclerView() {
        recyclerView.apply {
            layoutManager = gridLayoutManager
            addItemDecoration(gridItemDecoration)
            adapter = this@MainActivity.adapter
        }
    }


    override fun onHourOfDayUpdated(hourWithMeridiem: String) {
        txtHour.text = hourWithMeridiem
    }


    override fun onResume() {
        super.onResume()
        presenter.setView(this)
        presenter.getAnimals()
        if (presenter.isLiveOneDayEnabled())
            chronometer.start()
    }


    override fun onPause() {
        chronometer.stop()
        presenter.detachView()
        super.onPause()
    }


    override fun onAnimalsCreated(animals: MutableList<Animal>) {
        addDataToAdapter(animals)
    }


    private fun addDataToAdapter(animals: MutableList<Animal>) {
        clearAdapter()
        adapter.add(animals)
        adapter.notifyDataSetChanged()
    }


    override fun onDayChanged(animals: MutableList<Animal>) {
        clearAdapter()
        addDataToAdapter(animals)
        showFriendshipsChangedSnackbar()
    }


    private fun clearAdapter() {
        adapter.clear()
    }


    private fun showFriendshipsChangedSnackbar() {
        Snackbar.make(constraintRoot, "Changed friendships!", Snackbar.LENGTH_SHORT).show()
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        menuItemLOD = menu.findItem(R.id.actionLiveOneDay)
        return true
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.getItemId()
        when (id) {
             R.id.actionLiveOneDay -> {
                 presenter.changeLiveOneDayStatus()
                 return true
             }
        }
        return super.onOptionsItemSelected(item)
    }


    override fun disableLiveOneDay() {
        menuItemLOD!!.title = getString(R.string.enable_live_one_day)
        chronometer.stop()
        hideHour()
    }


    private fun hideHour() {
        txtHour.animate().translationY(-200F)
    }


    override fun enableLiveOneDay() {
        menuItemLOD!!.title = getString(R.string.disable_live_one_day)
        showHour()
        chronometer.start()
    }


    private fun showHour() {
        txtHour.animate().translationY( 0F )
    }

}
