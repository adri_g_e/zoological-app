package com.age.zoologicalapp.ui

import com.age.zoologicalapp.data.Animal


interface View {

    fun onAnimalsCreated(animals: MutableList<Animal>)
    fun onDayChanged(animals: MutableList<Animal>)
    fun onHourOfDayUpdated(hourWithMeridiem: String)
    fun enableLiveOneDay()
    fun disableLiveOneDay()
}