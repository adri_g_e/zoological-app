package com.age.zoologicalapp.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.age.zoologicalapp.R
import com.age.zoologicalapp.data.Animal
import kotlinx.android.synthetic.main.item_animal.view.*


class AnimalsAdapter : RecyclerView.Adapter<AnimalsAdapter.AnimalViewHolder>() {


    private val animals: MutableList<Animal> = mutableListOf()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AnimalViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_animal, parent, false)
        return AnimalViewHolder(view)
    }

    override fun onBindViewHolder(holder: AnimalViewHolder, position: Int) {
        val animal = animals[position]
        holder.set(animal)
    }


    override fun getItemCount() = animals.size


    fun add(animals: MutableList<Animal>) {
        this.animals.addAll(animals)
    }

    fun clear() {
        this.animals.clear()
    }


    inner class AnimalViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun set(animal: Animal) {
            itemView.imgAnimal.setImageURI(animal.photoPath)
            itemView.txtAnimalName.text = animal.name
            itemView.txtAnimalProperties.text = animal.propertiesToString()
            itemView.txtAnimalFriendships.text = animal.friendListToString()
        }
    }
}