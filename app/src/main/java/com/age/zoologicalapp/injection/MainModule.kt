package com.age.zoologicalapp.injection

import com.age.zoologicalapp.components.MainModel
import com.age.zoologicalapp.components.MainPresenter
import com.age.zoologicalapp.components.Model
import com.age.zoologicalapp.components.Presenter
import dagger.Module
import dagger.Provides


@Module
class MainModule {

    @Provides
    fun provideMainPresenter(model: Model) : Presenter {
        return MainPresenter(model)
    }


    @Provides
    fun provideMainModel() : Model {
        return MainModel()
    }

}