package com.age.zoologicalapp.injection

import com.age.zoologicalapp.ui.MainActivity
import dagger.Component


@Component(modules = [MainModule::class])
interface MainComponent {

    fun inject(activity: MainActivity)

}