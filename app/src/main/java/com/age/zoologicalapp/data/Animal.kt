package com.age.zoologicalapp.data

import android.util.Log
import java.lang.StringBuilder



abstract class Animal constructor(var name: String, var favoriteFood: String, var photoPath: String) {

    private val TAG = Animal::class.simpleName
    var friendships = mutableListOf<Animal>()


    abstract fun propertiesToString(): String


    fun addFriendship(animal: Animal) {
        this.friendships.add(animal)
        Log.i(TAG, "$name has established friendship with ${animal.name}")
    }


    fun removeFriendship(animal: Animal) {
        friendships.remove(animal)
        Log.i(TAG, "$name has lost frienship with ${animal.name}")
    }


    fun friendListToString() : String {
        if (friendships.isEmpty())
            return "Without friends"

        val strFriendsBuilder = StringBuilder().appendln("Friends: ")
        friendships.forEach { animalFriend ->
            strFriendsBuilder.appendln("- ${animalFriend.name}")
        }
        return strFriendsBuilder.toString()
    }

}