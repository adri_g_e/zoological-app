package com.age.zoologicalapp.data

import java.lang.StringBuilder


class Parrot(name: String,
             favoriteFood: String,
             var canSpeak: Boolean,
             lenghtOfWings: Double,
             photoPath: String) : Bird(name, favoriteFood, lenghtOfWings, photoPath) {

    private lateinit var strBuilder : StringBuilder


    override fun propertiesToString(): String {
        initializeBuilder()
        strBuilder.appendln("- Favorite food: $favoriteFood")
        strBuilder.appendln("- Lenght of wings: $lenghtOfWings")
        appendSpeakProperty()
        return strBuilder.toString()
    }


    private fun initializeBuilder() {
        strBuilder = StringBuilder()
    }


    private fun appendSpeakProperty() {
        if (canSpeak)
            strBuilder.append("- Can speak")
        else
            strBuilder.append("- Can't speak")
    }

}