package com.age.zoologicalapp.data

import java.lang.StringBuilder


class Chicken constructor(name: String,
                          favoriteFood: String,
                          lenghtOfWings: Double,
                          var isBroiler: Boolean,
                          photoPath: String) : Bird(name, favoriteFood, lenghtOfWings, photoPath) {

    private lateinit var strBuilder : StringBuilder


    override fun propertiesToString(): String {
        initializeBuilder()
        strBuilder.appendln("- Favorite food: $favoriteFood")
        strBuilder.appendln("- Lenght of wings: $lenghtOfWings")
        appendBroilerProperty()
        return strBuilder.toString()
    }


    private fun initializeBuilder() {
        strBuilder = StringBuilder()
    }


    private fun appendBroilerProperty() {
        if (isBroiler)
            strBuilder.append("- Is broiler")
        else
            strBuilder.append("- Is not broiler")
    }

}