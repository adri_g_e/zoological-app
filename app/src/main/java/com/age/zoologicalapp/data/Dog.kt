package com.age.zoologicalapp.data


class Dog constructor(name: String,
                      favoriteFood: String,
                      var type: String,
                      photoPath: String) : Animal(name, favoriteFood, photoPath) {


    private lateinit var strBuilder : java.lang.StringBuilder


    override fun propertiesToString(): String {
        initializeBuilder()
        strBuilder.appendln("- Favorite food: $favoriteFood")
        strBuilder.append("- Dog type: $type")
        return strBuilder.toString()
    }


    private fun initializeBuilder() {
        strBuilder = java.lang.StringBuilder()
    }

}