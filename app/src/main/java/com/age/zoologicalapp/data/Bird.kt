package com.age.zoologicalapp.data


abstract class Bird constructor(name: String,
                                favoriteFood: String,
                                var lenghtOfWings: Double,
                                photoPath: String) : Animal(name, favoriteFood, photoPath) {

}