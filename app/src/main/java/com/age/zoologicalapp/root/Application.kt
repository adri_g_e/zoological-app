package com.age.zoologicalapp.root

import android.app.Application
import com.facebook.drawee.backends.pipeline.Fresco


class Application : Application() {

    override fun onCreate() {
        super.onCreate()
        initializeApp()
    }


    private fun initializeApp() {
        Fresco.initialize(this)
    }
}