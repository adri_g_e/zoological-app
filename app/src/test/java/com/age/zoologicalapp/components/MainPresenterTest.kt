package com.age.zoologicalapp.components

import com.age.zoologicalapp.ui.View
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.*



class MainPresenterTest {

    private lateinit var presenter: Presenter
    private lateinit var mockedModel: Model
    private lateinit var mockedView: View


    @Before
    fun setUp() {
        mockedModel = mock<Model>(Model::class.java)
        mockedView = mock<View>(View::class.java)
        presenter = MainPresenter(mockedModel)
        presenter.setView(mockedView)
    }


    @Test
    fun testIfCreateAnimalsIsCalledOnPresenterGetAnimals() {
        presenter.getAnimals()
        verify(mockedModel, times(1)).createAnimals()
    }


    @Test
    fun checkThatModelIsNotAccessedWhenLiveOneDayStatusIsChanged() {
        presenter.changeLiveOneDayStatus()
        verifyZeroInteractions(mockedModel)
    }


    @Test
    fun isLiveOneDayitShouldReturnBoolean() {
        val mockedPresenter = mock<Presenter>(Presenter::class.java)
        `when`(mockedPresenter.isLiveOneDayEnabled()).thenReturn(true)
    }



    // TODO Implement all Test ...


    @After
    fun tearDown() {
        presenter.detachView()
    }
}