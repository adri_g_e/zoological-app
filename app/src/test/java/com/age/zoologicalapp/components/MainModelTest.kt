package com.age.zoologicalapp.components

import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.`when`



class MainModelTest {

    private lateinit var mockedModel: Model


    @Before
    fun setUp() {
        mockedModel = Mockito.mock<Model>(Model::class.java)
    }

    @Test
    fun testCreateAnimalsReturnAnimalMutableList() {
        `when`(mockedModel.createAnimals()).thenReturn(mutableListOf())
    }


    // TODO Implement all Test ...

}