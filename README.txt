ARQUITECTURA ------------------------------------------------------------------------------------------------------------------------

Este proyecto implementa el patrón de arquitectura MVP, organizando los componentes/clases en distintos packages según su función/responsabilidad.
Mediante paquetes se separa la UI de la lógica de negocio de UI (situada en el presentador) y la lógica de negocio de modelo.
Utiliza dagger para la inyección de dependencias y así poder invertirlas.

DESCRIPCIÓN APP ---------------------------------------------------------------------------------------------------------------------

Al arrancar la app tiene el modo "Live one day" activado. Este modo simula que las horas del día van pasando (1 seg. == 1 hora) y se muestran en la UI.
Durante la última hora del día todos los animales cambian de amistades. Los cambios se actualizan en la UI y se imprimen en en Log.

TEST --------------------------------------------------------------------------------------------------------------------------------

Faltan varios test unitarios/integración y todos los de UI con Espresso.